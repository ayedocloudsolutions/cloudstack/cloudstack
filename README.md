# Cloudstack

## Installation

See [Installation](https://docs.cloudstack.one/getting-started).

![Cloudstack](docs/logo.png)

Cloudstack is a plugin-based framework for hyperconverged container infrastructure. You can start with a single node for development and scale to a production-grade system along the way. Cloudstack can create infrastructure and setup Kubernetes for you, too. It's built around industry best practices and mostly unopinionated, allowing you to deploy your cloud native workloads in the way that fits best for you while giving you a concise framework for painless operations. Cloudstack comes with a suite of well-known and tightly integrated tools that you can leverage to support your software development process. It's designed to work with standard Kubernetes clusters and thus enables a common workflow for development and production deployments.

Learn more on [docs.cloudstack.one](https://docs.cloudstack.one).

## Support

If you have problems or questions or if you're interested in enterprise support please [reach out](https://www.ayedo.de).

Cloudstack works great with our other products:

- [ns0](https://ns0.co)
- [Polycrate](https://docs.polycrate.sh)
- [ohMyHelm](https://docs.ohmyhelm.sh)
- [Microplatforms](https://microplatforms.de)

## Development

See [Development](https://docs.cloudstack.one/development).

## Authors

© 2022 [Ayedo Cloud Solutions GmbH](https://www.ayedo.de)

## License

This project is licensed under Apache 2 license. For the full text of the license, see the [LICENSE](LICENSE) file.