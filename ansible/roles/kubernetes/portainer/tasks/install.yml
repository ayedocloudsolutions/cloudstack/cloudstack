# - name: asserting credentials
#   include_role:
#     name: helpers/credential_secret
#   vars:
#     credential_user: admin

- name: get admin credentials
  include_role:
    name: helpers/get-credentials
  vars:
    plugin: "portainer"
    type: "admin-credentials"
    result_var: "portainer_admin_credentials"
    create: True

- name: installing module
  include_role:
    name: helpers/install_helm
  vars:
    release_values: "{{ lookup('template', 'values.yml.j2') | from_yaml }}"

- name: Get JWT
  block:
    - name: waiting for portainer to become ready
      community.kubernetes.k8s_info:
        api_version: v1
        kind: Pod
        label_selectors:
          - "app.kubernetes.io/name=portainer"
          - "app.kubernetes.io/instance=portainer"
        namespace: portainer
        wait: True
        wait_sleep: 10
        wait_timeout: 10
        wait_condition:
          status: True
          type: Ready
      register: portainer_ready

    - name: starting Portainer api proxy
      include_role:
        name: helpers/k8s_port_forward
      vars:
        target: "service/portainer"
        remote_port: 9000
        local_port: 9000
        result_var: "portainer_port_forward"

    - name: setting Portainer api endpoint
      set_fact:
        portainer_api_endpoint: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}"
      when: portainer_port_forward == ""

    - name: setting Portainer api endpoint
      set_fact:
        portainer_api_endpoint: http://localhost:9000
      when: portainer_port_forward != ""

    - name: checking if Portainer is ready
      uri:
        url: "{{ portainer_api_endpoint }}/api/status"
        method: GET
      register: _result
      until: _result.status == 200
      retries: 15
      delay: 10

    # - name: asserting portainer admin secret
    #   community.kubernetes.k8s_info:
    #     api_version: v1
    #     kind: Secret
    #     name: "portainer-admin-secret"
    #     namespace: "{{ cloudstack.namespace }}"
    #   register: portainer_admin_secret

    # - name: showing portainer admin secret
    #   debug:
    #     msg: "{{ portainer_admin_secret }}"
    #     verbosity: 1

    # - name: retrieving portainer admin secret
    #   set_fact:
    #     portainer_admin_secret_value: "{{ portainer_admin_secret['resources'][0]['data']['secret'] }}"
    #   when: portainer_admin_secret.resources | length > 0

    # - name: setting portainer admin secret
    #   block:
    #     - name: generating portainer admin secret
    #       set_fact:
    #         portainer_admin_secret_value: "{{ lookup('password', '/dev/null chars=ascii_lowercase,digits length=16') }}"

    #     - name: creating portainer admin secret
    #       k8s:
    #         state: present
    #         definition:
    #           apiVersion: v1
    #           kind: Secret
    #           type: Opaque
    #           metadata:
    #             name: "portainer-admin-secret"
    #             namespace: "{{ cloudstack.namespace }}"
    #             annotations: "{{ default_annotations }}"
    #             labels: "{{ default_labels }}"
    #           data:
    #             secret: "{{ portainer_admin_secret_value | b64encode }}"
    #   when: portainer_admin_secret.resources | length == 0

    - name: setting inital user
      uri:
        url: "{{ portainer_api_endpoint }}/api/users/admin/init"
        method: POST
        body:
          {
            "Username": "{{ portainer_admin_credentials.user }}",
            "Password": "{{ portainer_admin_credentials.password }}",
          }
        status_code: [200, 201, 202]
        body_format: json
      ignore_errors: yes
      failed_when: False

    # If this task fails, all following tasks will be skipped as auth won't be possible
    - name: getting Portainer JWT token
      uri:
        url: "{{ portainer_api_endpoint }}/api/auth"
        method: POST
        body:
          {
            "Username": "{{ portainer_admin_credentials.user }}",
            "Password": "{{ portainer_admin_credentials.password }}",
          }
        status_code: 200
        body_format: json
        return_content: yes
      register: portainer_token
      ignore_errors: yes
  when: lookup('env', 'K8S_DISTRO') != "docker-desktop"

- name: integrating with Keycloak
  block:
    - name: asserting client
      include_role:
        name: helpers/client_secret
      vars:
        create_secret: True

    # - name: asserting Keycloak credentials
    #   include_role:
    #     name: paas/helpers/credential_secret
    #   vars:
    #     create_secret: False
    #     plugin: keycloak
    #   when: keycloak_admin_credentials is undefined

    - name: get keycloak admin credentials
      include_role:
        name: helpers/get-credentials
      vars:
        plugin: "keycloak"
        type: "admin-credentials"
        result_var: "keycloak_admin_credentials"
        create: True
      when: keycloak_admin_credentials is undefined

    # - name: asserting client secret
    #   community.kubernetes.k8s_info:
    #     api_version: v1
    #     kind: Secret
    #     name: "portainer-keycloak-client-secret"
    #     namespace: "{{ cloudstack.namespace }}"
    #   register: client_secret

    # - name: showing client secret
    #   debug:
    #     msg: "{{ client_secret }}"
    #     verbosity: 1

    # - name: retrieving client secret
    #   set_fact:
    #     portainer_client_secret_value: "{{ client_secret['resources'][0]['data']['secret'] }}"
    #   when: client_secret.resources | length > 0

    # - name: generating client secret
    #   set_fact:
    #     portainer_client_secret_value: "{{ lookup('password', '/dev/null chars=ascii_lowercase,digits length=16') }}"
    #   when: client_secret.resources | length == 0

    # - name: creating client secret
    #   k8s:
    #     state: present
    #     definition:
    #       apiVersion: v1
    #       kind: Secret
    #       type: Opaque
    #       metadata:
    #         name: "portainer-keycloak-client-secret"
    #         namespace: "acs"
    #         annotations: "{{ default_annotations }}"
    #       data:
    #         secret: "{{ portainer_client_secret_value | b64encode }}"
    #   when: client_secret.resources | length == 0

    - name: creating Keycloak client
      community.general.keycloak_client:
        auth_client_id: admin-cli
        auth_keycloak_url: "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth"
        auth_realm: master
        auth_username: "{{ keycloak_admin_credentials.user }}"
        auth_password: "{{ keycloak_admin_credentials.password }}"
        state: present
        realm: master
        client_id: "portainer"
        name: "portainer"
        description: "Client for Portainer at {{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}"
        root_url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}"
        base_url: "/"
        enabled: True
        client_authenticator_type: client-secret
        secret: "{{ client_secret_secret }}"
        redirect_uris:
          - "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/*"
        #web_origins:
        #  - "https://portainer.{{ cloudstack.stack.hostname }}"
        #not_before: 1507825725
        bearer_only: False
        consent_required: False
        standard_flow_enabled: True
        implicit_flow_enabled: False
        direct_access_grants_enabled: True
        service_accounts_enabled: False
        authorization_services_enabled: False
        public_client: False
        frontchannel_logout: False
        protocol: openid-connect
        full_scope_allowed: false
        node_re_registration_timeout: -1
        protocol_mappers:
          - config:
              access.token.claim: True
              claim.name: "roles"
              id.token.claim: True
              usermodel.clientRoleMapping.clientId: "portainer"
              jsonType.label: String
              user.attribute: roles
              userinfo.token.claim: True
              multivalued: True
            name: Roles
            protocol: openid-connect
            protocolMapper: oidc-usermodel-client-role-mapper
      register: portainer_keycloak_client

    - name: debug portainer_keycloak_client
      debug:
        msg: "{{ portainer_keycloak_client }}"
        verbosity: 1

    - name: creating admin role for client
      community.general.keycloak_role:
        name: admin
        realm: master
        client_id: portainer
        state: present
        auth_client_id: admin-cli
        auth_keycloak_url: "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth"
        auth_realm: master
        auth_username: "{{ keycloak_admin_credentials.user }}"
        auth_password: "{{ keycloak_admin_credentials.password }}"
      register: client_role

    - name: getting auth token
      uri:
        url: "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/realms/master/protocol/openid-connect/token"
        method: POST
        body:
          grant_type: "password"
          username: "{{ keycloak_admin_credentials.user }}"
          password: "{{ keycloak_admin_credentials.password }}"
          client_id: "admin-cli"
        status_code: 200
        body_format: form-urlencoded
      register: keycloak_admin_auth_token

    - name: debug auth token
      debug:
        msg: "{{ keycloak_admin_auth_token }}"
        verbosity: 1

    - name: getting Keycloak user list
      uri:
        url: "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/admin/realms/master/users"
        method: GET
        body_format: json
        status_code: 200
        headers:
          Authorization: "bearer {{ keycloak_admin_auth_token.json.access_token }}"
      register: keycloak_user_list

    - name: debug keycloak user list
      debug:
        msg: "{{ keycloak_user_list.json }}"
        verbosity: 1

    - name: getting id of user admin
      set_fact:
        admin_id: "{{ item.id }}"
      with_items: "{{ keycloak_user_list.json }}"
      when: item.username == "admin"

    - name: adding admin user to client admin role
      uri:
        url: "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/admin/realms/master/users/{{ admin_id }}/role-mappings/clients/{{ portainer_keycloak_client.end_state.id }}"
        method: POST
        body:
          - clientRole: True
            id: "{{ client_role.end_state.id }}"
            name: "admin"
        body_format: json
        status_code:
          - 200
          - 201
          - 202
          - 203
          - 204
        headers:
          Authorization: "bearer {{ keycloak_admin_auth_token.json.access_token }}"

    - name: debug client_role
      debug:
        msg: "{{ client_role }}"
        verbosity: 1

    # - name: Map a client role to a group, authentication with credentials
    #   community.general.keycloak_client_rolemapping:
    #     realm: master
    #     auth_client_id: admin-cli
    #     auth_keycloak_url: "https://login.{{ cloudstack.stack.hostname }}/auth"
    #     auth_realm: master
    #     auth_username: "{{ keycloak_admin_credentials.user }}"
    #     auth_password: "{{ keycloak_admin_credentials.password }}"
    #     state: present
    #     client_id: portainer
    #     group_name: admin
    #     roles:
    #       - name: admin
    #         id: "{{ client_role.end_state.id }}"

    - name: Set Portainer oAuth
      when: portainer_token.json.jwt is defined
      block:
        - name: get current settings
          uri:
            url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/settings"
            method: GET
            body_format: json
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body:
          register: portainer_settings

        - name: Show current settings
          debug:
            msg: "{{ portainer_settings.json }}"
            verbosity: 1

        - name: Set oAuth settings to Portainer
          uri:
            url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/settings"
            method: PUT
            body_format: json
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body: >
              {
                "oauthSettings": {
                  "AccessTokenURI": "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/realms/master/protocol/openid-connect/token",
                  "AuthorizationURI": "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/realms/master/protocol/openid-connect/auth",
                  "ClientID": "portainer",
                  "ClientSecret": "{{ portainer_client_secret_value }}",
                  "DefaultTeamID": 1,
                  "LogoutURI": "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/realms/master/protocol/openid-connect/token",
                  "OAuthAutoCreateUsers": true,
                  "RedirectURI": "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}",
                  "ResourceURI": "{{ ingress_protocol }}://{{ cloudstack.plugins.keycloak.subdomain }}.{{ cloudstack.stack.hostname }}/auth/realms/master/protocol/openid-connect/userinfo",
                  "SSO": true,
                  "Scopes": "email roles",
                  "UserIdentifier": "email"
                }
              }

    - name: setting teams and roles
      when: portainer_token.json.jwt is defined
      block:
        - name: create default team
          uri:
            url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/teams"
            method: POST
            body_format: json
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body: >
              {
                "name": "default"
              }
          register: portainer_team

        - name: create default endpoint group
          uri:
            url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/endpoint_groups"
            method: POST
            body_format: json
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body: >
              {
                "associatedEndpoints": [],
                "description": "default_group",
                "name": "autocreatet default group",
                "tagIDs": []
              }
          register: portainer_endpoint_group

        - name: add team to default group
          uri:
            url: "{{ ingress_protocol }}://{{ cloudstack.plugins.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/endpoint_groups/{{ portainer_endpoint_group.json.Id }}"
            method: PUT
            body_format: json
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body: >
              {
                "teamAccessPolicies": {
                  "{{ portainer_team.json.Id }}": {
                    "RoleId": 0
                  }
                }
              }
          register: portainer_endpoint_group_team

        - name: create kubernetes endpoint and assign endpoint group
          uri:
            url: "{{ ingress_protocol }}://{{ plugins.cloudstack.portainer.subdomain }}.{{ cloudstack.stack.hostname }}/api/endpoints"
            method: POST
            body_format: form-multipart
            status_code: 200
            return_content: true
            headers:
              Accept: application/json
              Authorization: "Bearer {{ portainer_token.json.jwt }}"
            body:
              Name: "{{ cloudstack.stack.name }}"
              EndpointCreationType: "5"
              GroupID: "{{ portainer_endpoint_group.json.Id }}"
              TLS: "true"
              TLSSkipVerify: "true"
              TLSSkipClientVerify: "true"
          register: portainer_endpoint
  when: "'keycloak' in cloudstack.stack.plugins"
