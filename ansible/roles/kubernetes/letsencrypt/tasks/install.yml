- name: configuring issuers
  block:
    - name: configuring letsencrypt
      block:
        - name: asserting issuer mail
          set_fact:
            issuer_mail: "{{ cloudstack.plugins.letsencrypt.config.mail | default(cloudstack.stack.mail, True) }}"

        - name: asserting conditions
          assert:
            that:
              - issuer_mail != ""
            msg: "Either letsencrypt.mail or stack.mail must be set"
          when: cloudstack.plugins.letsencrypt.config.issuers.http01.enabled|bool

        - name: waiting for cert-manager to become ready
          community.kubernetes.k8s_info:
            api_version: v1
            kind: Pod
            label_selectors:
              - "app.kubernetes.io/name=cert-manager"
              - "app.kubernetes.io/component=controller"
            namespace: "{{ cloudstack['plugins']['cert_manager']['namespace'] }}"
            wait: True
            wait_sleep: 10
            wait_timeout: 10
            wait_condition:
              status: True
              type: Ready
          register: cert_manager_ready

        - name: debug
          debug:
            msg: "{{ cert_manager_ready }}"
            verbosity: 1

        - name: configuring dns01 issuer
          block:
            - name: adding cloudflare secret
              community.kubernetes.k8s:
                state: present
                definition:
                  apiVersion: v1
                  kind: Secret
                  metadata:
                    name: cloudflare-api-key-secret
                    namespace: cert-manager
                  type: Opaque
                  stringData:
                    api-key: "{{ cloudstack.plugins.cloudflare.config.api.token }}"
              when:
                - cloudstack.plugins.letsencrypt.config.issuers.dns01.solver == "cloudflare"
                - cloudstack.plugins.letsencrypt.config.issuers.dns01.provider == "letsencrypt"

            - name: adding dns01 issuer
              community.kubernetes.k8s:
                state: present
                definition: "{{ lookup('template', 'dns01-issuer.yml.j2') | from_yaml }}"
          when: cloudstack.plugins.letsencrypt.config.issuers.dns01.enabled|bool

        # - name: adding acs certificate
        #   community.kubernetes.k8s:
        #     state: present
        #     definition: "{{ lookup('template', 'acs-cert.yml.j2') | from_yaml }}"
        #   when: cloudstack.cert_manager.solvers.dns01.enabled|bool

        - name: configuring http01 issuer
          block:
            - name: adding letsencrypt issuer
              community.kubernetes.k8s:
                state: present
                definition:
                  apiVersion: cert-manager.io/v1
                  kind: ClusterIssuer
                  metadata:
                    name: "letsencrypt-{{ environment.name }}"
                    namespace: "cert-manager"
                  spec:
                    acme:
                      server: "{{ environment.server }}"
                      email: "{{ issuer_mail }}"
                      privateKeySecretRef:
                        name: "letsencrypt-{{ environment.name }}-issuer"
                      solvers:
                        - http01:
                            ingress:
                              class: nginx
              with_items: "{{ cloudstack.plugins.letsencrypt.config.environments }}"
              loop_control:
                loop_var: environment
              when: cloudstack.plugins.letsencrypt.config.issuers.http01.provider == "letsencrypt"
          when: cloudstack.plugins.letsencrypt.config.issuers.http01.enabled|bool
  when: cloudstack.stack.tls|bool

- name: adding custom CA for inter-service communication
  block:
    - name: fail - this is deprecated
      fail:
        msg: "The custom CA is deprecated. Please disable it and see https://docs.cloudstack.one for more information."
    - name: Create cert-manager directory
      ansible.builtin.file:
        path: "{{ context }}/cert-manager"
        recurse: yes
        state: directory

    - name: create cert-manager CA Key
      command: "openssl ecparam -genkey -name prime256v1 -noout -out {{ context }}/cert-manager/ca.key"
      args:
        creates: "{{ context }}/cert-manager/ca.key"

    - name: create cert-manager CA Certificate
      command: "openssl req -x509 -new -nodes -key {{ context }}/cert-manager/ca.key -sha256 -days 3650 -subj '/CN={{ cloudstack.stack.name }}' -out {{ context }}/cert-manager/ca.crt -extensions v3_ca -config {{ role_path }}/files/openssl-with-ca.cnf"
      args:
        creates: "{{ context }}/cert-manager/ca.crt"

    - name: adding cert-manager CA secret
      community.kubernetes.k8s:
        state: present
        definition:
          apiVersion: v1
          kind: Secret
          type: tls
          metadata:
            name: "akp-custom-ca"
            namespace: "cert-manager"
          data:
            tls.crt: "{{ lookup('file', context+'/cert-manager/ca.crt') | string | b64encode }}"
            tls.key: "{{ lookup('file', context+'/cert-manager/ca.key') | string | b64encode }}"

    - name: adding CA issuer
      community.kubernetes.k8s:
        state: present
        definition:
          apiVersion: cert-manager.io/v1
          kind: ClusterIssuer
          metadata:
            name: cloudstack-ca-issuer
            namespace: "cert-manager"
          spec:
            ca:
              secretName: cloudstack-custom-ca
  when: cloudstack.plugins.cert_manager.config.ca_issuer.enabled|bool
