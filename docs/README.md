---
title: About
description: A framework for hyperconverged container infrastructure
---

# Cloudstack

Cloudstack is a plugin-based framework for hyperconverged container infrastructure. You can start with a single node for development and scale to a production-grade system along the way.

<figure markdown>
  ![Cloudstack](logo.png){: style="height:300px;width:300px;"}
  <figcaption>Hans-Gert the Gopher, Cloudstack's mascot</figcaption>
</figure>

Cloudstack can create infrastructure and setup Kubernetes for you, too. It's built around industry best practices and mostly unopinionated, allowing you to deploy your cloud native workloads in the way that fits best for you while giving you a concise framework for painless operations. Cloudstack comes with a suite of well-known and tightly integrated tools that you can leverage to support your software development process. It's designed to work with standard Kubernetes clusters and Docker systems and thus enables a common workflow for development and production deployments.

## Features

- Create your systems with **infrastructure as code** on top of a best-practice platform framework and a complete cloud native tool runtime delivered in a container
- Build reusable [plugins](reference.md#plugins) and [commands](reference.md#commands) for easy infrastructure management in the language of your choice
- Design portable [stacks](reference.md#stack) composed of [core plugins](reference.md#core-plugins) and [custom plugins](reference.md#custom-plugins) that are tailored to your specific needs
- Collaborate on infrastructure as a team with native protocols like [git](https://git-scm.com/)
- Automate the creation and management of cloud infrastructure at [Hetzner](plugins/infrastructure/hcloud_vms.md) and [Azure](plugins/infrastructure/azure_aks.md)
- Build and operate container clusters with [Kubernetes](plugins/kubernetes/k3s.md), [Docker](plugins/docker/docker.md) or [Docker Swarm](plugins/docker/docker_swarm.md)
- Install and manage container applications with the Cloudstack CLI or tools like [Lens](https://k8slens.dev/) or [Portainer](https://www.portainer.io/)
- Get high-available storage for your containers with [Longhorn](plugins/kubernetes/longhorn.md)
- Monitor your applications with state-of-the-art tooling like [Prometheus](plugins/kubernetes/prometheus.md), [Loki](plugins/docker/loki.md), [Tempo](plugins/kubernetes/tempo.md) and [Grafana](plugins/docker/grafana.md)
- Automate TLS and traffic routing with [Traefik](plugins/docker/traefik.md), [nginx](plugins/kubernetes/nginx_ingress.md) and [Letsencrypt](plugins/kubernetes/letsencrypt.md)
- Secure your networks with [Wireguard](plugins/linux/wireguard.md) and [Cilium](plugins/kubernetes/cilium_cni.md)
- Automate your application deployments with [ArgoCD](plugins/kubernetes/argocd.md), [Portainer](plugins/docker/portainer.md) and [external-dns](plugins/kubernetes/external_dns.md)
- Secure your applications with [Keycloak](plugins/kubernetes/keycloak.md) or [Authentik](plugins/docker/authentik.md)
- Enjoy a first-class integrations with Ansible, Docker and Kubernetes

[Get started](install.md){ .md-button .md-button--primary }
