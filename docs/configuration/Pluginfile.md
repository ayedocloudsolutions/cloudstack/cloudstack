# Pluginfile

Plugin configuration can be offloaded to a `Pluginfile` within the [plugin directory](reference.md#custom-plugins).

The Pluginfile contains only the relevant configuration of a plugin just like it would have been added to the Stackfile.

```yaml
# Stackfile
plugins:
  foo:
    config:
      bar: baz
```

```yaml
# plugins/foo/Pluginfile
config:
  bar: baz
```