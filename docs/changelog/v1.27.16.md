
<a name="v1.27.16"></a>
## [v1.27.16](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/compare/v1.27.15...v1.27.16) (2022-06-02)

### Bug Fixes

* cert_manager_crds

