
<a name="v1.27.18"></a>
## [v1.27.18](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/compare/v1.27.17...v1.27.18) (2022-06-02)

### Bug Fixes

* hostname omitempty

