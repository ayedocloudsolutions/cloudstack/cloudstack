# grafana

- Code: [docker/grafana](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/grafana)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_grafana.yml
  uninstall:
    script:
      - ansible-playbook docker_grafana.yml
version: latest
```