# traefik

- Code: [docker/traefik](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/traefik)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_traefik.yml
  uninstall:
    script:
      - ansible-playbook docker_traefik.yml
version: latest
```