# authentik

- Code: [docker/authentik](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/authentik)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_authentik.yml
  uninstall:
    script:
      - ansible-playbook docker_authentik.yml
version: 2.13.1
config:
  integrations:
    traefik: false
  port: 9000
  db:
    host: postgresql
    user: authentik
    database: authentik
    password: authentik
  redis:
    host: redis
  email:
    user: ""
    password: ""
    host: ""
    port: ""
    ssl: False
    tls: True
  sitename: ""
```