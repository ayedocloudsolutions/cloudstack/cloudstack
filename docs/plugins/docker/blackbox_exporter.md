# blackbox_exporter

- Code: [docker/blackbox_exporter](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/blackbox_exporter)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_blackbox_exporter.yml
  uninstall:
    script:
      - ansible-playbook docker_blackbox_exporter.yml
version: latest
config:
  targets: []
  dns: 8.8.8.8
```