# cadvisor

- Code: [docker/cadvisor](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/cadvisor)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_cadvisor.yml
  uninstall:
    script:
      - ansible-playbook docker_cadvisor.yml
version: latest
```