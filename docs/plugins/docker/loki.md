# loki

- Code: [docker/loki](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/loki)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_loki.yml
  uninstall:
    script:
      - ansible-playbook docker_loki.yml
version: latest
```