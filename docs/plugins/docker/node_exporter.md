# node_exporter

- Code: [docker/node_exporter](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/node_exporter)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_node_exporter.yml
  uninstall:
    script:
      - ansible-playbook docker_node_exporter.yml
version: latest
```