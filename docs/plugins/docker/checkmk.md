# checkmk

- Code: [docker/checkmk](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/checkmk)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_checkmk.yml
  uninstall:
    script:
      - ansible-playbook docker_checkmk.yml
version: 2.0.0-latest
config:
  integrations:
    traefik: false
  sitename: ""
  port: 5000
```