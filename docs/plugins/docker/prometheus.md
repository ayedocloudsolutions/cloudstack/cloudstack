# prometheus

- Code: [docker/prometheus](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/prometheus)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_prometheus.yml
  uninstall:
    script:
      - ansible-playbook docker_prometheus.yml
version: v2.31.1
config:
  integrations:
    traefik: false
    alertmanager: false
    node_exporter: true
    blackbox_exporter: true
    cadvisor: true
  sitename: ""
  port: 9090
```