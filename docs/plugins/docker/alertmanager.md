# alertmanager

- Code: [docker/alertmanager](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/alertmanager)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_alertmanager.yml
  uninstall:
    script:
      - ansible-playbook docker_alertmanager.yml
version: latest
```