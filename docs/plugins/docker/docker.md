# docker

- Code: [docker/docker_docker](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/docker)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_docker.yml
  uninstall:
    script:
      - ansible-playbook docker_docker.yml
version: latest
```