# portainer

- Code: [docker/portainer](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/portainer)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_portainer.yml
  uninstall:
    script:
      - ansible-playbook docker_portainer.yml
version: 2021.12.4
config:
  integrations:
    traefik: false
  sitename: ""
  port: 9000
```