# swarm

- Code: [docker/swarm](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/swarm)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_swarm.yml
  uninstall:
    script:
      - ansible-playbook docker_swarm.yml
version: latest
```