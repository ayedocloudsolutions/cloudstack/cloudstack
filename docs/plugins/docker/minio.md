# minio

- Code: [docker/minio](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/docker/minio)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook docker_minio.yml
  uninstall:
    script:
      - ansible-playbook docker_minio.yml
version: latest
```