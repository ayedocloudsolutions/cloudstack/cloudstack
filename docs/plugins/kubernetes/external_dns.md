# external_dns

- Code: [kubernetes/external_dns](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/external_dns)

`external_dns` installs [external-dns](https://github.com/kubernetes-sigs/external-dns) on a Kubernetes cluster. external-dns picks up hostnames from ingress and service objects and adjusts the respective DNS zone (here's a list of [supported providers](https://github.com/kubernetes-sigs/external-dns#status-of-providers))

!!! note
    The default provider for external-dns in Cloudstack is `cloudflare` which can be configured through the [cloudflare](plugins/cloudflare.md) plugin.

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_external_dns.yml
  uninstall:
    script:
      - ansible-playbook k8s_external_dns.yml
namespace: external-dns
subdomain: ""
version: 5.4.4
```