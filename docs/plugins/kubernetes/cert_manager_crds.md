# cert_manager_crds

- Code: [kubernetes/cert_manager_crds](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/cert_manager_crds)

`cert_manager_crds` is used to deploy `cert-manager`'s Custom Resource Definitions to a Kubernetes cluster. This is needed for [kubernetes/cert_manager](plugins/kubernetes/cert_manager) to run.