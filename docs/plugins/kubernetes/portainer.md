# portainer

- Code: [kubernetes/portainer](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/portainer)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_portainer.yml
  uninstall:
    script:
      - ansible-playbook k8s_portainer.yml
namespace: portainer
subdomain: portainer
version: 2.9.2
```