# kubeapps

- Code: [kubernetes/kubeapps](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/kubeapps)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_kubeapps.yml
  uninstall:
    script:
      - ansible-playbook k8s_kubeapps.yml
namespace: kubeapps
subdomain: kubeapps
version: 7.3.2
```