# letsencrypt

- Code: [kubernetes/letsencrypt](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/letsencrypt)

This plugin sets up the necessary Kubernetes objects to connect [cert_manager](plugins/kubernetes/cert_manager) with [Let's Encrypt](https://letsencrypt.org/). By default, this plugin works with LetsEncrypt's staging API to avoid issues with blacklisting. To use the production API (i.e. to get valid certificates), change the value of `config.environment` to `prod`.

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_letsencrypt.yml
  uninstall:
    script:
      - ansible-playbook k8s_letsencrypt.yml
namespace: cloudstack
version: latest
config:
  environment: staging|prod (default: staging)
  environments:
    - name: staging
      server: https://acme-staging-v02.api.letsencrypt.org/directory
    - name: prod
      server: https://acme-v02.api.letsencrypt.org/directory
  issuers:
    dns01:
      enabled: false
      provider: letsencrypt
      solver: cloudflare
      zone: ""
    http01:
      enabled: true
      provider: letsencrypt
  mail: ""
```

## Default environment changed in 1.30.0

With `cloudstack` 1.30.0, the default `environment` for LetsEncrypt changed from `prod` to `staging` to mitigate issues with rate limits. Stacks deployed from older versions of `cloudstack` need to adapt their Stackfile accordingly to match the legacy default behaviour:

```yaml
plugins:
  letsencrypt:
    environment: true
```