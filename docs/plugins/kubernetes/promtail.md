# promtail

- Code: [kubernetes/promtail](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/promtail)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_promtail.yml
  uninstall:
    script:
      - ansible-playbook k8s_promtail.yml
namespace: promtail
version: 3.8.2
```