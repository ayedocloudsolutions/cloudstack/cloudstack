# tempo

- Code: [kubernetes/tempo](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/tempo)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_tempo.yml
  uninstall:
    script:
      - ansible-playbook k8s_tempo.yml
namespace: tempo
version: 0.2.6
config:
  retention: 24h
```