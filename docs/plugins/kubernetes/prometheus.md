# prometheus

- Code: [kubernetes/prometheus](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/prometheus)

Installs Prometheus, Grafana and Alertmanager into a Kubernetes cluster.

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_prometheus.yml
  uninstall:
    script:
      - ansible-playbook k8s_prometheus.yml
namespace: monitoring
subdomain: ""
version: 19.2.2
config:
  retention: 10d
  receiver: ""
  remote_write:
    enabled: false
    targets:
      - authorization:
          credentials: {}
          type: Bearer
        basicAuth:
          password: ""
          username: ""
        bearerToken: ""
        bearerTokenFile: ""
        headers:
          X-Scope-OrgID: ""
        name: default
        oauth2: {}
        remoteTimeout: 30s
        sendExemplars: false
        url: http://prometheus.example.com
        writeRelabelConfigs: ""
```