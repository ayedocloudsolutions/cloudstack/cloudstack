# longhorn

- Code: [kubernetes/longhorn](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/longhorn)

## Directories used

- /var/lib/rancher/longhorn/ (VOLUME DATA)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_longhorn.yml
  uninstall:
    script:
      - ansible-playbook k8s_longhorn.yml
namespace: longhorn-system
version: 1.2.2
config:
  datapath: /var/lib/longhorn/
```

## Troubleshooting

- [`MountVolume.SetUp failed for volume` due to multipathd on the node](https://longhorn.io/kb/troubleshooting-volume-with-multipath/)
- [Pod stuck in creating state when Longhorn volumes filesystem is corrupted](https://longhorn.io/kb/troubleshooting-volume-filesystem-corruption/)
- If provisioning of volumes fails continuously, it often helps to kill the pods of the `csi-attacher` and `csi-provisioner` DaemonSets. Once they're recreated, the Kubernetes API can interface with the CSI driver again
