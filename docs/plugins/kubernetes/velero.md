# velero

- Code: [kubernetes/velero](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/velero)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_velero.yml
  uninstall:
    script:
      - ansible-playbook k8s_velero.yml
namespace: velero
version: 2.27.0
config:
  credentials:
    id: ""
    key: ""
  backup_location:
    name: ""
    bucket: ""
    region: ""
    force_path_style: true
    url: ""
  scheduled_backups:
    disabled: true
    namespaces: []
    ttl: ""
    schedule: ""
    include_crds: false
  snapshotLocation: {}
```