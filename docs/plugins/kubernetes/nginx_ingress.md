# nginx_ingress

- Code: [kubernetes/nginx_ingress](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/nginx_ingress)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_nginx_ingress.yml
  uninstall:
    script:
      - ansible-playbook k8s_nginx_ingress.yml
namespace: nginx-ingress
version: 3.36.0
```