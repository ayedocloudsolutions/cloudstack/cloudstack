# argocd

- Code: [kubernetes/argocd](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/argocd)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_argocd.yml
  uninstall:
    script:
      - ansible-playbook k8s_argocd.yml
namespace: argocd
subdomain: argocd
version: 2.0.4
artifacts: []
config: {}
```