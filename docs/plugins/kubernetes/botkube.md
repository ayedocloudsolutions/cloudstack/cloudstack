# botkube

- Code: [kubernetes/botkube](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/botkube)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_botkube.yml
  uninstall:
    script:
      - ansible-playbook k8s_botkube.yml
namespace: botkube
subdomain: ""
version: v0.12.4
config:
  communications:
    mattermost:
      botname: botkube
      channel: ""
      enabled: false
      team: ""
      token: ""
      url: ""
  config:
    resources: []
    settings:
      clustername: ""
      kubectl:
        enabled: true
```