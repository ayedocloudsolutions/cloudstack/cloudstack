# keycloak

- Code: [kubernetes/keycloak](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/keycloak)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_keycloak.yml
  uninstall:
    script:
      - ansible-playbook k8s_keycloak.yml
namespace: keycloak
subdomain: keycloak
version: 18.1.1
config:
  admin:
    password: ""
    user: ""
```