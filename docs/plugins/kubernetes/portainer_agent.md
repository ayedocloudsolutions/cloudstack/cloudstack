# portainer_agent

- Code: [kubernetes/portainer_agent](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/portainer_agent)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_portainer_agent.yml
  uninstall:
    script:
      - ansible-playbook k8s_portainer_agent.yml
namespace: portainer-agent
version: 2.9.0
```