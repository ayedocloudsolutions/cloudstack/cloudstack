# prometheus_crds

- Code: [kubernetes/prometheus_crds](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/prometheus_crds)

Installs the Prometheus Custom Resource Definitions into a Kubernetes cluster.
