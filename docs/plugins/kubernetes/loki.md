# loki

- Code: [kubernetes/loki](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/loki)

## Configuration

```yaml
# defaults
config:
  retention: 672h
commands:
  install:
    script:
      - ansible-playbook k8s_loki.yml
  uninstall:
    script:
      - ansible-playbook k8s_loki.yml
namespace: loki
version: 2.6.0
```

## Troubleshooting

### No space left on device (Longhorn)

- Scale Loki's StatefulSet to 0 and force-delete all pods stuck in Terminating state
- In the Longhorn UI, wait for the Loki Volume to become detached
- Once detached, click the settings icon at the end of the row and select "Expand volume"
- Set an appropriate size and confirm
- Once the volume is resized (must have state "Detached") you can scale the StatefulSet up to 1 again

