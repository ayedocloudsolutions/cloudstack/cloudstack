# eventrouter

- Code: [kubernetes/eventrouter](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/kubernetes/eventrouter)

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook k8s_eventrouter.yml
  uninstall:
    script:
      - ansible-playbook k8s_eventrouter.yml
namespace: kube-system
subdomain: ""
version: latest
```