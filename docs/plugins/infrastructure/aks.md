# aks

- Code: [infrastructure/aks](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/infrastructure/aks)

Provisions an AKS cluster on [Azure Cloud](https://azure.microsoft.com/en-us/services/kubernetes-service/#overview). Generates a [kubeconfig](reference.md#kubeconfig) in the [context](reference.md#context) directory that can be consumed by plugins like [prometheus](plugins/kubernetes/prometheus.md).

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook infra_aks.yml
  uninstall:
    script:
      - ansible-playbook infra_aks.yml
config:
  kubernetes_version: 1.24.3
  rbac: 
    enabled: true
  pools:
    default:
      count: 1
      size: Standard_B2s
      type: VirtualMachineScaleSets
      mode: System
    user:
      count: 3
      size: Standard_D2_v2
      mode: User
      type: VirtualMachineScaleSets
  resource_group: ""
  location: "Germany West Central"
  tenant_id: "" # required
  subscription_id: "" # required
  serviceprincipal:
    client_id: "" # required
    secret: "" # required
  tags: {}
```