# hcloud_vms

- Code: [infrastructure/hcloud_vms](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/infrastructure/hcloud_vms)

Provisions virtual machines on [HETZNER Cloud](https://www.hetzner.com/de/cloud). Generates an [inventory](reference.md#inventory) in the [context](reference.md#context) directory - this is an Ansible-compatible inventory that can be consumed by plugins like [k3s](plugins/kubernetes/k3s.md).

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook infra_hcloud_vms.yml
  uninstall:
    script:
      - ansible-playbook infra_hcloud_vms.yml
config:
  inventory:
    path: inventory.yml
  master:
    count: 1
    location: fsn1
    type: cx31
  node:
    count: 3
    location: fsn1
    type: cx31
  os_image: ubuntu-20.04
  token: "" # the HETZNER API token
```