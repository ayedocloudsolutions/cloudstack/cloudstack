# azure_aks

- Code: [infrastructure/azure_aks](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/-/tree/main/ansible/roles/infrastructure/azure_aks)

Provisions an AKS cluster on [Azure Cloud](https://azure.microsoft.com/en-us/services/kubernetes-service/#overview). Generates a [kubeconfig](reference.md#kubeconfig) in the [context](reference.md#context) directory that can be consumed by plugins like [prometheus](plugins/kubernetes/prometheus.md).

## Configuration

```yaml
# defaults
commands:
  install:
    script:
      - ansible-playbook infra_azure_aks.yml
  uninstall:
    script:
      - ansible-playbook infra_azure_aks.yml
config:
  node:
    count: 3
    location: Germany West Central
    type: Standard_D2_v2
  os_image: ubuntu-20.04
  resource_group: ""
  serviceprincipal:
    client:
      id: ""
      secret: ""
  tags: {}
```