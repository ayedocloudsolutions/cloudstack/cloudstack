---
title: Install
description: Install the Cloudstack
---

# Install

## Automated installer

Using the Cloudstack installer automates the process of downloading and moving the `cloudstack` binary to your `$PATH`. The installer automatically detects your operating system and architecture.

=== "curl"

    **real** quick:

    ```bash
    curl https://docs.cloudstack.one/get-cloudstack.sh | bash
    ```

    a bit safer:

    ```bash
    curl -fsSL -o get-cloudstack.sh https://docs.cloudstack.one/get-cloudstack.sh
    chmod 0700 get-cloudstack.sh
    # Optionally: less get-cloudstack.sh
    ./get-cloudstack.sh
    ```

=== "wget"

     **real** quick:

    ```bash
    wget -qO- https://docs.cloudstack.one/get-cloudstack.sh | bash
    ```

    a bit safer:

    ```bash
    wget -q -O get-cloudstack.sh https://docs.cloudstack.one/get-cloudstack.sh
    chmod 0700 get-cloudstack.sh
    # Optionally: less get-cloudstack.sh
    ./get-cloudstack.sh
    ```

## Manual Download

Install the [CLI](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack) by following the steps for your platform below:

=== "Linux"

    ``` bash
    export VERSION=1.25
    curl -fsSLo cloudstack.tar.gz https://s3.ayedo.de/cloudstack/cli/v${VERSION}/cloudstack_${VERSION}_linux_amd64.tar.gz
    tar xvzf cloudstack.tar.gz
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "Linux (ARM)"

    ``` bash
    export VERSION=1.25
    curl -fsSLo cloudstack.tar.gz https://s3.ayedo.de/cloudstack/cli/v${VERSION}/cloudstack_${VERSION}_linux_arm64.tar.gz
    tar xvzf cloudstack.tar.gz
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "macOS"

    ``` bash
    export VERSION=1.25
    curl -fsSLo cloudstack.tar.gz https://s3.ayedo.de/cloudstack/cli/v${VERSION}/cloudstack_${VERSION}_darwin_amd64.tar.gz
    tar xvzf cloudstack.tar.gz
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "macOS (M1)"

    ``` bash
    export VERSION=1.25
    curl -fsSLo cloudstack.tar.gz https://s3.ayedo.de/cloudstack/cli/v${VERSION}/cloudstack_${VERSION}_darwin_arm64.tar.gz
    tar xvzf cloudstack.tar.gz
    chmod +x cloudstack
    ./cloudstack version
    ```