/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// installCmd represents the install command
var launchCmd = &cobra.Command{
	// Deprecated: "This command is deprecated as it has limited functionality. Use `cloudstack pipelines` instead",
	Use:   "launch",
	Short: "Run the install command for all plugins in Stackfile.stack.plugins",
	Args:  cobra.ExactArgs(0),
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		log.Fatal("This command is deprecated as it has limited functionality. Use `cloudstack pipelines run install` instead. See https://docs.cloudstack.one/reference#pipelines for more information")
		// loadStackfile()
		// if pull {
		// 	PullContainerImage(imageRef, imageVersion)
		// }

		// if len(args) == 0 {
		// 	for _, plugin := range cloudstackConfig.GetStringSlice("stack.plugins") {
		// 		pluginsCmd.Run(cmd, []string{plugin, "install"})
		// 		log.Info("Waiting 15 seconds before next plugin call to avoid race conditions.")
		// 		time.Sleep(15 * time.Second)
		// 	}
		// } else {
		// 	log.Error("No arguments allowed")
		// 	os.Exit(1)
		// }
	},
}

func init() {
	rootCmd.AddCommand(launchCmd)
}
