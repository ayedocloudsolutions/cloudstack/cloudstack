package cmd

import (
	"github.com/go-playground/validator/v10"
)

type promptContent struct {
	errorMsg string
	label    string
}

type StackConfig struct {
	Name     string   `yaml:"name" mapstructure:"name" json:"name" validate:"required"`
	Hostname string   `yaml:"hostname" mapstructure:"hostname" json:"hostname" `
	Tls      bool     `yaml:"tls" mapstructure:"tls" json:"tls"`
	Sso      bool     `yaml:"sso,omitempty" mapstructure:"sso,omitempty" json:"sso,omitempty"`
	Plugins  []string `yaml:"plugins" mapstructure:"plugins" json:"plugins"`
	Exposed  bool     `yaml:"exposed" mapstructure:"exposed" json:"exposed"`
}

type ImageConfig struct {
	Reference string `yaml:"reference,omitempty" mapstructure:"reference,omitempty" json:"reference,omitempty"`
	Version   string `yaml:"version,omitempty" mapstructure:"version,omitempty" json:"version,omitempty"`
}

type Stackfile struct {
	Namespace        string                    `yaml:"namespace,omitempty" mapstructure:"namespace,omitempty" json:"namespace,omitempty"`
	Legacy_namespace string                    `yaml:"legacy_namespace,omitempty" mapstructure:"legacy_namespace,omitempty" json:"legacy_namespace,omitempty"`
	Image            ImageConfig               `yaml:"image,omitempty" mapstructure:"image,omitempty" json:"image,omitempty"`
	Stack            StackConfig               `yaml:"stack,omitempty" mapstructure:"stack,omitempty" json:"stack,omitempty"`
	Plugins          map[string]PluginConfig   `yaml:"plugins,omitempty" mapstructure:"plugins,omitempty" json:"plugins,omitempty"`
	Pipelines        map[string]PipelineConfig `yaml:"pipelines,omitempty" mapstructure:"pipelines,omitempty" json:"pipelines,omitempty"`
}

type ChartRepoConfig struct {
	Url  string `yaml:"url,omitempty" mapstructure:"url,omitempty" json:"url,omitempty"`
	Name string `yaml:"name,omitempty" mapstructure:"name,omitempty" json:"name,omitempty"`
}

type ChartConfig struct {
	Name    string          `yaml:"name,omitempty" mapstructure:"name,omitempty" json:"name,omitempty"`
	Version string          `yaml:"version,omitempty" mapstructure:"version,omitempty" json:"version,omitempty"`
	Url     string          `yaml:"url,omitempty" mapstructure:"url,omitempty" json:"url,omitempty"`
	Repo    ChartRepoConfig `yaml:"repo,omitempty" mapstructure:"repo,omitempty" json:"repo,omitempty"`
}

type ArtifactConfig struct {
	Name     string `yaml:"name,omitempty" mapstructure:"name,omitempty" json:"name,omitempty"`
	Filename string `yaml:"filename,omitempty" mapstructure:"filename,omitempty" json:"filename,omitempty"`
	Url      string `yaml:"url,omitempty" mapstructure:"url,omitempty" json:"url,omitempty"`
}

type TlsCert struct {
	Content string `yaml:"content,omitempty" mapstructure:"content,omitempty" json:"content,omitempty"`
	Path    string `yaml:"path,omitempty" mapstructure:"path,omitempty" json:"path,omitempty"`
}
type TlsKey struct {
	Content string `yaml:"content,omitempty" mapstructure:"content,omitempty" json:"content,omitempty"`
	Path    string `yaml:"path,omitempty" mapstructure:"path,omitempty" json:"path,omitempty"`
}

type TlsSecretConfig struct {
	Cert TlsCert `yaml:"cert,omitempty" mapstructure:"cert,omitempty" json:"cert,omitempty"`
	Key  TlsKey  `yaml:"key,omitempty" mapstructure:"key,omitempty" json:"key,omitempty"`
	Name string  `yaml:"name,omitempty" mapstructure:"name" json:"name" validate:"required"`
}

type TlsConfig struct {
	Enabled bool `yaml:"enabled" mapstructure:"enabled" json:"enabled"`
	// if provider == secret, look into the Secret section
	// if provider == letsencrypt,etc, look into the respective plugin section
	Provider string          `yaml:"provider,omitempty" mapstructure:"provider,omitempty" json:"provider,omitempty"`
	Secret   TlsSecretConfig `yaml:"secret,omitempty" mapstructure:"secret,omitempty" json:"secret,omitempty"`
}

type IngressConfig struct {
	Enabled  bool      `yaml:"enabled" mapstructure:"enabled" json:"enabled"`
	Hostname string    `yaml:"hostname" mapstructure:"hostname" json:"hostname"`
	Tls      TlsConfig `yaml:"tls,omitempty" mapstructure:"tls,omitempty" json:"tls,omitempty"`
}

type BackupConfig struct {
	Enabled bool `yaml:"enabled" mapstructure:"enabled" json:"enabled"`
}

type ProxyConfig struct {
	Target      string `yaml:"target,omitempty" mapstructure:"target,omitempty" json:"target,omitempty"`
	Local_Port  string `yaml:"local_port,omitempty" mapstructure:"local_port,omitempty" json:"local_port,omitempty"`
	Remote_Port string `yaml:"remote_port,omitempty" mapstructure:"remote_port,omitempty" json:"remote_port,omitempty"`
}
type PluginConfig struct {
	Enabled   bool                   `yaml:"enabled" mapstructure:"enabled" json:"enabled"`
	Commands  map[string]Command     `yaml:"commands" mapstructure:"commands" json:"commands"`
	Namespace string                 `yaml:"namespace,omitempty" mapstructure:"namespace,omitempty" json:"namespace,omitempty"`
	Subdomain string                 `yaml:"subdomain,omitempty" mapstructure:"subdomain,omitempty" json:"subdomain,omitempty"`
	Version   string                 `yaml:"version,omitempty" mapstructure:"version" json:"version" validate:"required"`
	Proxy     []ProxyConfig          `yaml:"proxy,omitempty" mapstructure:"proxy,omitempty" json:"proxy,omitempty"`
	Backup    BackupConfig           `yaml:"backup,omitempty" mapstructure:"backup,omitempty" json:"backup,omitempty"`
	Ingress   IngressConfig          `yaml:"ingress,omitempty" mapstructure:"ingress,omitempty" json:"ingress,omitempty"`
	Chart     ChartConfig            `yaml:"chart,omitempty" mapstructure:"chart,omitempty" json:"chart,omitempty"`
	Artifacts []ArtifactConfig       `yaml:"artifacts,omitempty" mapstructure:"artifacts,omitempty" json:"artifacts,omitempty"`
	Needs     []string               `yaml:"needs,omitempty" mapstructure:"needs,omitempty" json:"needs,omitempty"`
	Provides  []string               `yaml:"provides,omitempty" mapstructure:"provides,omitempty" json:"provides,omitempty"`
	Rejects   []string               `yaml:"rejects,omitempty" mapstructure:"rejects,omitempty" json:"rejects,omitempty"`
	Config    map[string]interface{} `yaml:"config,omitempty" mapstructure:"config,omitempty,remain" json:"config,omitempty"`
}

type Command struct {
	Interactive bool     `yaml:"interactive,omitempty" mapstructure:"interactive,omitempty" json:"interactive,omitempty"`
	Script      []string `yaml:"script,omitempty" mapstructure:"script,omitempty" json:"script,omitempty"`
}

func (c *Stackfile) Validate() error {
	return validator.New().Struct(c)
}
