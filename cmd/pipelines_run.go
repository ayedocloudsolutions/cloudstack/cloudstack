/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/gobwas/glob"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var dryRunPipeline bool
var fromStep int
var toStep int

var runPipelineCmd = &cobra.Command{
	Use:   "run",
	Short: "Run pipeline",
	Long:  `Run pipeline`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 && args[0] != "" {
			pipeline = args[0]
		} else {
			pipeline = "default"
		}
		loadStackfile()

		runPipeline(cmd, pipeline)
	},
}

func init() {
	runPipelineCmd.Flags().IntVar(&step, "step", -1, "Single step of the pipeline to be executed")
	runPipelineCmd.Flags().IntVar(&fromStep, "from-step", -1, "Step from which the pipeline should start")
	runPipelineCmd.Flags().IntVar(&toStep, "to-step", -1, "Step up to which the pipeline should run")
	runPipelineCmd.Flags().BoolVarP(&dryRunPipeline, "dry-run", "d", false, "Step of the pipeline to be executed")
	pipelinesCmd.AddCommand(runPipelineCmd)
}

func runPipeline(cmd *cobra.Command, pipeline string) {
	// Check if pipeline exists
	log.Info("Running pipeline ", pipeline)
	if cloudstack.Pipelines[pipeline].Steps != nil {
		for index, pipelineStep := range cloudstack.Pipelines[pipeline].Steps {
			if step != -1 {
				// A specific step has been set with --step 1 (or 2, 3, ...)
				// Check if current step is the right step to execute
				// if not, continue
				if step != index {
					continue
				}
			} else {
				if fromStep != -1 {
					if index < fromStep {
						// Don't run this, it's too early
						log.Debugf("Not running step %d (%s) as we didn't reach --from-step (%d) yet", index, pipelineStep.Name, fromStep)
						continue
					}
				}

				if toStep != -1 {
					if index > toStep {
						// Don't run this, it's too late
						log.Debugf("Not running step %d (%s) as we already reached --to-step (%d) yet", index, pipelineStep.Name, toStep)
						continue
					}
				}
			}

			log.Info("Running step ", pipelineStep.Name)

			discoverKubeconfig()

			saveRuntimeStackfile()

			// Generate Plugin Regex
			cl := CloudstackLog{}

			plugin = pipelineStep.Plugin
			pluginCommand = pipelineStep.Command

			g := glob.MustCompile(plugin)
			for index := range cloudstack.Plugins {
				if g.Match(index) {
					err := callPlugin(index, pipelineStep.Command)
					if err != nil {
						cl.ExitCode = 1
						cl.Append(cmd, err.Error())
						log.Fatal(err.Error())
					}
					cl.ExitCode = 0
					cl.Append(cmd, "Command successful")
				}
			}

		}
	} else {
		log.Fatal("No steps defined")
	}
}
