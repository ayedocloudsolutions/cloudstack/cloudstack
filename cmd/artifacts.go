package cmd

import (
	"errors"

	log "github.com/sirupsen/logrus"
)

func (c ArtifactConfig) Download(fp string) error {
	if c.Url != "" {
		log.Debug("Found URL: " + c.Url)
		err := DownloadFile(c.Url, fp)
		return err
	} else {
		return errors.New("no URL given")
	}
}
