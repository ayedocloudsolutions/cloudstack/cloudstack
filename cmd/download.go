/*
Copyright © 2021 Fabian Peter <fp@ayedo.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var downloadDir string

// installCmd represents the install command
var downloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Initalize a Cloudstack",
	Long:  `Initalize a Cloudstack`,
	Run: func(cmd *cobra.Command, args []string) {
		loadDefaults()

		downloadHelmCharts()
		downloadArtifacts()
	},
}

func init() {
	downloadCmd.PersistentFlags().StringVar(&downloadDir, "download-dir", cwd, "Target directory for downloads")
	rootCmd.AddCommand(downloadCmd)
}

func downloadHelmCharts() {
	for plugin, pluginConfig := range cloudstack.Plugins {
		if pluginConfig.Chart.Url != "" {
			fp := filepath.Join(downloadDir, plugin+"-"+pluginConfig.Version+".tgz")
			err := pluginConfig.Chart.Download(fp)
			CheckErr(err)
		}
	}
}

func downloadArtifacts() {
	for plugin, pluginConfig := range cloudstack.Plugins {
		if len(pluginConfig.Artifacts) > 0 {
			for _, artifact := range pluginConfig.Artifacts {
				if artifact.Url != "" {
					dp := filepath.Join(downloadDir, plugin)

					// Create download path if not existing
					err := os.MkdirAll(dp, os.ModePerm)
					CheckErr(err)

					// Download file
					fp := filepath.Join(dp, artifact.Filename)
					err = artifact.Download(fp)
					CheckErr(err)
				}
			}
		}
	}
	// wget -O "cert_manager/cert-manager-crds.yml" -q "https://github.com/jetstack/cert-manager/releases/download/v${CERT_MANAGER_VERSION}/cert-manager.crds.yaml"
}
