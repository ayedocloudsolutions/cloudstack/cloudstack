package cmd

import (
	_ "embed"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var validate *validator.Validate

//go:embed defaults.yml
var defaultsYml []byte

var context string
var defaults = viper.New()
var cloudstackConfig = viper.New()
var stackfile string
var runtimeStackfile string
var kubeconfig string
var k8sDistro string
var k8sOnline bool = false
var pluginCommand string
var imageRef string
var imageVersion string
var cloudstackVersion string
var logLevel string
var pull bool
var force bool
var interactive bool
var devDir string
var command string
var plugins string
var plugin string
var pluginRoot string = "plugins"
var cwd, _ = os.Getwd()
var callUUID = getCallUUID()
var gitConfig = viper.New()
var step int

var inventory string
var inventoryLocalPath string = "/tmp/inventory.yml"
var inventoryContainerPath string = "/tmp/inventory.yml"
var inventoryConfigObject = viper.New()

//var version = BuildVersion(cwd)
var version = "development"
var home, _ = homedir.Dir()
var overrides []string
var workdir string = "/cloudstack"
var outputFormat string

var cloudstack Stackfile
var pipeline string
