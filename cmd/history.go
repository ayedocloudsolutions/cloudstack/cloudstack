package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-git/go-git/v5"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

var logCmd = &cobra.Command{
	// Deprecated: "This command is deprecated as it has limited functionality. Use `cloudstack pipelines` instead",
	Use:   "log 'this is a logmessage'",
	Short: "Adds a message to the log file",
	Args:  cobra.ExactArgs(1),
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		message := args[0]

		log := CloudstackLog{}
		log.Append(cmd, message)
	},
}

func init() {
	rootCmd.AddCommand(logCmd)
}

type CloudstackLog struct {
	Msg      string `yaml:"msg,omitempty" mapstructure:"msg,omitempty" json:"msg,omitempty"`
	Command  string `yaml:"command,omitempty" mapstructure:"command,omitempty" json:"command,omitempty"`
	ExitCode int    `yaml:"exit_code,omitempty" mapstructure:"exit_code,omitempty" json:"exit_code,omitempty"`
	Commit   string `yaml:"commit,omitempty" mapstructure:"commit,omitempty" json:"commit,omitempty"`
	Version  string `yaml:"version,omitempty" mapstructure:"version,omitempty" json:"version,omitempty"`
}

func (l *CloudstackLog) SetCommand(cmd *cobra.Command) *CloudstackLog {

	commandPath := cmd.CommandPath()
	localArgs := cmd.Flags().Args()

	localFlags := []string{}

	cmd.Flags().Visit(func(flag *pflag.Flag) {
		//fmt.Printf("--%s=%s\n", flag.Name, flag.Value)
		localFlags = append(localFlags, fmt.Sprintf("--%s=%s", flag.Name, flag.Value))
	})

	command := strings.Join([]string{
		commandPath,
		strings.Join(localArgs, " "),
		strings.Join(localFlags, " "),
	}, " ")

	l.Command = command
	return l
}

func (l *CloudstackLog) GetCommit() {
	r, err := git.PlainOpen(context)
	if err != nil {
		// No repo
		l.Commit = ""
		return
	}
	ref, err := r.Head()
	if err != nil {
		return
	}

	commit, err := r.CommitObject(ref.Hash())
	if err != nil {
		return
	}
	l.Commit = commit.Hash.String()

}

func (l *CloudstackLog) Append(cmd *cobra.Command, message string) *CloudstackLog {
	l.Msg = message
	l.Version = version
	l.SetCommand(cmd)
	l.GetCommit()

	json, err := json.Marshal(l)
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.OpenFile(filepath.Join(context, "cloudstack.log"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	_, err = f.Write(json)
	if err != nil {
		log.Fatal(err)
	}

	if _, err = f.WriteString("\n"); err != nil {
		log.Fatal(err)
	}

	return nil
}
