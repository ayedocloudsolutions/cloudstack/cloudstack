/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// installCmd represents the install command
var sshCmd = &cobra.Command{
	Use:   "ssh",
	Short: "SSH into a node",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		loadStackfile()
		loadInventory()

		var node string
		var command []string

		// cloudstack ssh -> enters the first node in the list of all nodes
		// cloudstack ssh $INVENTORY_HOSTNAME -> enters the node with the given hostname
		// cloudstack ssh $INVENTORY_HOSTNAME "ps aux" -> executes ps aux on the node with the given hostname

		// load inventory
		// search for node in list of hosts

		if len(args) == 0 {
			// enter the first node
			node = "master-0"
			command = []string{}
		} else if len(args) == 1 {
			// enter a specific node
			node = args[0]
			command = []string{}
		} else if len(args) == 2 {
			// execute a specific command on a specific node
			node = args[0]
			command = args[1:]
		} else {
			log.Fatal("Incorrect number of args")
		}

		log.Debug(command)

		interactive = true
		connectWithSSH(node, command)
		// load inventory
	},
}

func init() {
	rootCmd.AddCommand(sshCmd)
}

func loadInventory() {
	_inventoryPath := filepath.Join(context, "inventory.yml")
	if _, err := os.Stat(_inventoryPath); os.IsNotExist(err) {
		log.Fatal("inventory.yml not found. Please add an inventory.")
	} else {
		inventory = _inventoryPath
	}

	log.Debug("Loading inventory from " + inventory)
	inventoryConfigObject.SetConfigFile(inventory)
	inventoryConfigObject.SetConfigType("yaml")

	err := inventoryConfigObject.MergeInConfig()
	CheckErr(err)
}

func connectWithSSH(node string, cmd []string) {
	var nodeInfo *viper.Viper
	var nodePath string

	// find node as is
	nodePath = "all.hosts." + node
	if !inventoryConfigObject.IsSet(nodePath) {
		// compile new node ID
		nodeLong := strings.Join([]string{cloudstack.Stack.Name, node}, "-")
		nodePath = "all.hosts." + nodeLong
		if !inventoryConfigObject.IsSet("all.hosts." + nodeLong) {
			log.Fatal("Node " + node + " and " + nodeLong + " not found")
		} else {
			log.Info("Found node " + nodeLong)
		}
	} else {
		log.Info("Found node " + node)
	}

	nodeInfo = inventoryConfigObject.Sub(nodePath)

	// set ssh params
	var sshUser string
	if nodeInfo.IsSet("ansible_ssh_user") {
		sshUser = nodeInfo.GetString("ansible_ssh_user")
	} else if nodeInfo.IsSet("ansible_user") {
		sshUser = nodeInfo.GetString("ansible_user")
	} else {
		sshUser = "root"
	}

	var sshPort string
	if nodeInfo.IsSet("ansible_ssh_port") {
		sshPort = nodeInfo.GetString("ansible_ssh_port")
	} else if cloudstack.Plugins["ssh"].Config["port"] != "" {
		portStr := fmt.Sprintf("%v", cloudstack.Plugins["ssh"].Config["port"])
		sshPort = portStr
	} else {
		sshPort = "22"
	}

	var sshHost string
	if nodeInfo.IsSet("ansible_host") {
		sshHost = nodeInfo.GetString("ansible_host")
	} else {
		log.Fatal("ansible_host not set")
	}

	sshPrivateKey := filepath.Join(context, "id_rsa")

	args := []string{
		"-l",
		sshUser,
		"-i",
		sshPrivateKey,
		"-p",
		sshPort,
		sshHost,
	}

	if len(cmd) != 0 {
		args = append(args, cmd...)
		log.Debug("ssh -l " + sshUser + " -i " + sshPrivateKey + " -p " + sshPort + " " + sshHost + " " + strings.Join(cmd, " "))
	} else {
		log.Debug("ssh -l " + sshUser + " -i " + sshPrivateKey + " -p " + sshPort + " " + sshHost)
	}

	log.Info("Starting ssh session: " + sshUser + "@" + sshHost + ":" + sshPort)

	_, err := RunCommand("ssh", args...)
	CheckErr(err)
}
