package cmd

import (
	"bufio"
	"bytes"
	ctx "context"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"sort"
	"strings"
	"syscall"
	"time"

	goErrors "errors"

	"os"
	"os/exec"

	"github.com/google/uuid"
	"github.com/manifoldco/promptui"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"

	"github.com/go-playground/validator/v10"
)

func GetContainerEnv() []string {
	var _force string
	if force {
		_force = "1"
	} else {
		_force = "0"
	}
	env := []string{
		strings.Join([]string{"ANSIBLE_DISPLAY_SKIPPED_HOSTS", "no"}, "="),
		strings.Join([]string{"ANSIBLE_DISPLAY_OK_HOSTS", "yes"}, "="),
		strings.Join([]string{"ANSIBLE_HOST_KEY_CHECKING", "False"}, "="),
		strings.Join([]string{"ANSIBLE_INVENTORY", "/context/inventory.yml"}, "="),
		strings.Join([]string{"ANSIBLE_ROLES_PATH", "/root/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles:/cloudstack/roles"}, "="),
		strings.Join([]string{"ANSIBLE_COLLECTIONS_PATH", "/root/.ansible/collections:/usr/share/ansible/collections:/etc/ansible/collections"}, "="),
		strings.Join([]string{"ANSIBLE_VERBOSITY", logLevel}, "="),
		strings.Join([]string{"CLOUDSTACK_CLI_VERSION", version}, "="),
		strings.Join([]string{"CLOUDSTACK_CONTEXT", "/context"}, "="),
		strings.Join([]string{"CLOUDSTACK_DIR", "/cloudstack"}, "="),
		strings.Join([]string{"CLOUDSTACK_IMAGE_VERSION", imageVersion}, "="),
		strings.Join([]string{"CLOUDSTACK_IMAGE_REFERENCE", imageRef}, "="),
		strings.Join([]string{"CLOUDSTACK_PLUGIN", plugin}, "="),
		strings.Join([]string{"CLOUDSTACK_PLUGINS", plugins}, "="),
		strings.Join([]string{"CLOUDSTACK_PLUGIN_COMMAND", pluginCommand}, "="),
		strings.Join([]string{"CLOUDSTACK_PLUGIN_COMMAND_FORCE", _force}, "="),
		strings.Join([]string{"CLOUDSTACK_STACKFILE", "/tmp/Stackfile"}, "="),
		strings.Join([]string{"CLOUDSTACK_ORIGINAL_STACKFILE", stackfile}, "="),
		strings.Join([]string{"CLOUDSTACK_VERSION", cloudstackVersion}, "="),
		strings.Join([]string{"CLOUDSTACK_WORKDIR", workdir}, "="),
		strings.Join([]string{"KUBECONFIG", "/root/.kube/config"}, "="),
		strings.Join([]string{"K8S_DISTRO", k8sDistro}, "="),
		strings.Join([]string{"IN_CI", "True"}, "="),
		strings.Join([]string{"TERM", "xterm-256color"}, "="),
		//strings.Join([]string{"ANSIBLE_STDOUT_CALLBACK", "dense"}, "="),
		//strings.Join([]string{"DOCKER_HOST", os.Getenv("DOCKER_HOST")}, "="),
		// ANSIBLE_VERBOSITY
	}

	return env
}

const defaultFailedCode = 1

func RunCommand(name string, args ...string) (exitCode int, err error) {
	log.Debug("Running command: ", name, " ", strings.Join(args, " "))

	var cmd *exec.Cmd

	if !interactive {
		cmd = exec.Command(name, args...)

		var stdBuffer bytes.Buffer
		mw := io.MultiWriter(os.Stdout, &stdBuffer)

		cmd.Stdout = mw
		cmd.Stderr = mw

		err = cmd.Run()
	} else {
		cmd = exec.Command(name, args...)
		cmd.Stdout = os.Stdout
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr
		err = cmd.Run()
	}

	//log.Println(stdBuffer.String())

	if err != nil {
		// try to get the exit code
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			exitCode = ws.ExitStatus()
		} else {
			// This will happen (in OSX) if `name` is not available in $PATH,
			// in this situation, exit code could not be get, and stderr will be
			// empty string very likely, so we use the default fail code, and format err
			// to string and set to stderr
			log.Printf("Could not get exit code for failed program: %v, %v", name, args)
			exitCode = defaultFailedCode
		}
	} else {
		// success, exitCode should be 0 if go is ok
		ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitCode = ws.ExitStatus()
	}
	return exitCode, err
}

func PullContainerImage(image string, version string) error {

	args := []string{"pull", strings.Join([]string{image, version}, ":"), "-q"}
	_, err := RunCommand("docker", args...)
	CheckErr(err)

	return err
}

func RunContainer(image string, version string, command []string, env []string, mounts []string, pull bool, workdir string, ports []string) (int, error) {
	image = strings.Join([]string{image, version}, ":")

	// Prepare container command
	var runCmd []string

	// https://stackoverflow.com/questions/16248241/concatenate-two-slices-in-go
	runCmd = append(runCmd, []string{"run", "--rm", "-t"}...)

	// Env
	for _, envVar := range env {
		runCmd = append(runCmd, []string{"-e", envVar}...)
	}

	// Mounts
	for _, bindMount := range mounts {
		runCmd = append(runCmd, []string{"-v", bindMount}...)
	}

	// Ports
	for _, port := range ports {
		runCmd = append(runCmd, []string{"-p", port}...)
	}

	// Workdir
	runCmd = append(runCmd, []string{"--workdir", workdir}...)

	// Pull
	// if pull {
	// 	runCmd = append(runCmd, []string{"--pull", "always"}...)
	// } else {
	// 	runCmd = append(runCmd, []string{"--pull", "never"}...)
	// }

	// Interactive
	if interactive {
		log.Info("Running in interactive mode")
		runCmd = append(runCmd, []string{"-it"}...)
	}

	// Platform
	// fixed in cloudstack/cloudstack 1.1.3-main.build-46effead
	// Multi-platform images possible!
	// runCmd = append(runCmd, []string{"--platform", "linux/amd64"}...)

	// Entrypoint
	entrypointCmd := []string{"--entrypoint", "/bin/bash"}
	runCmd = append(runCmd, entrypointCmd...)

	// Image
	runCmd = append(runCmd, image)

	runCmd = append(runCmd, command...)

	// Run container
	exitCode, err := RunCommand("docker", runCmd...)

	return exitCode, err
}

func getKubeconfigPath(configDir string) string {
	var configPath string
	configFiles := []string{"kubeconfig.yml", "kubeconfig", "kubeconfig.yaml"}
	for _, file := range configFiles {
		path := filepath.Join(configDir, file)
		log.Debug("Looking for kubeconfig at ", path)
		if _, err := os.Stat(path); err == nil {
			// Kubeconfig found in the current directory
			configPath = path
			log.Debug("Found kubeconfig at ", path)
			return configPath
		}
	}
	log.Debug("Couldn't find kubeconfig")
	return ""
}

func discoverKubeconfig() error {
	// Additionally, check for KUBECONFIG env var
	kubeconfigEnv := os.Getenv("KUBECONFIG")

	if kubeconfigEnv != "" {
		kubeconfig = kubeconfigEnv
		log.Debug("Setting kubeconfig from KUBECONFIG env var to ", kubeconfigEnv)
	}

	log.Debug("Trying to find a kubeconfig in ", context)

	// Get stack kubeconfig
	stackKubeConfigPath := getKubeconfigPath(context)

	// Overwrite config if kubeconfig has been found in stack_dir
	if stackKubeConfigPath != "" {
		//viper.Set("kubeconfig", stackKubeConfigPath)
		kubeconfig = stackKubeConfigPath
		log.Debug("Setting kubeconfig to ", stackKubeConfigPath)
	}

	return nil
}

func getConfigPath(configDir string) string {
	var configPath string
	configFiles := []string{"Stackfile"}
	for _, file := range configFiles {
		path := filepath.Join(configDir, file)
		log.Debug("Looking for config at ", path)
		if _, err := os.Stat(path); err == nil {
			// ACS config found in the current directory
			// deduct stack from CWD name
			configPath = path
			log.Debug("Found config at ", path)
			return configPath
		}
	}
	log.Debug("Couldn't find config")

	return ""
}

func CheckErr(msg interface{}) {
	if msg != nil {
		log.Fatal(msg)
		os.Exit(1)
	}
}

func CreateStackDir(stackName string) (string, error) {
	var stackDir string = GetStackDir(stackName)

	err := os.MkdirAll(stackDir, os.ModePerm)
	CheckErr(err)

	return stackDir, nil
}

func CheckStackDirAvailable(stackDir string) bool {
	_, err := os.Stat(stackDir)
	return err != nil
}

func CheckStackExists(stackName string) bool {
	return CheckStackDirAvailable(stackName)
}

func GetStackDir(stackName string) string {
	acsDir := cloudstackConfig.GetString("acsDir")
	var stackDir string = filepath.Join(acsDir, stackName)

	return stackDir
}

func CheckKubeconfigExists(kubeconfig string) bool {
	_, err := os.Stat(kubeconfig)
	return err == nil
}

func DownloadFile(url string, fp string) error {
	// Create the file
	out, err := os.Create(fp)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	if resp.StatusCode == 404 {
		//log.Error("Download failed: file not found (404). URL: " + url)
		err = goErrors.New("Download failed: file not found (404). URL: " + url)
		return err
	}

	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	log.Debug("Downloaded file from ", url, " to ", fp)

	return nil
}

func getRemoteFileContent(url string) (string, error) {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err)
	}

	return string(b), err
}

func loadDefaults() {
	log.Debug("Loading defaults")
	defaults.SetConfigType("yaml")

	err := defaults.ReadConfig(bytes.NewBuffer(defaultsYml))
	if err != nil {
		log.Fatal(err)
	}

	unmarshalDefaults()

}

func loadStackfile() {
	//loadDefaults()

	// Check if Stackfile / acs.yml exists
	if stackfile == "" {
		_stackfilePath := filepath.Join(context, "Stackfile")
		if _, err := os.Stat(_stackfilePath); os.IsNotExist(err) {
			_acsPath := filepath.Join(context, "acs.yml")
			if _, err := os.Stat(_acsPath); err == nil {
				log.Warn("Stackfile not found. Found acs.yml instead. Please rename `acs.yml` to `Stackfile` or use --stackfile to specifiy it explicitly.")
				log.Warn("Run `mv " + _acsPath + " " + _stackfilePath + "` to fix the issue.")
				stackfile = _acsPath
			} else {
				log.Fatal("Stackfile not found. Please add a Stackfile or use --stackfile to specifiy a configuration file explicitly.")
			}
		} else {
			stackfile = _stackfilePath
			runtimeStackfile = stackfile
		}
	}

	// Check overrides
	if len(overrides) > 0 {
		for _, override := range overrides {
			// Split string by =
			kv := strings.Split(override, "=")

			// Override property
			log.Debug("Setting " + kv[0] + " to " + kv[1])
			cloudstackConfig.Set(kv[0], kv[1])
		}
	}

	// Load defaults
	//s, _ := yaml.Marshal(cloudstackDefaults)
	cloudstackConfig.SetConfigType("yaml")
	cloudstackConfig.ReadConfig(bytes.NewBuffer(defaultsYml))

	// Load vendor plugin configs
	var vendorPluginDirContent []fs.FileInfo

	vendorPluginPath := filepath.Join("/plugins")
	if _, err := os.Stat(vendorPluginPath); !os.IsNotExist(err) {
		vendorPluginDirContent, err = ioutil.ReadDir(vendorPluginPath)
		CheckErr(err)
		// Loop over plugins folder content
		for _, file := range vendorPluginDirContent {
			if file.IsDir() {
				// This is a plugin!
				pluginName := file.Name()
				log.Debug("Loading config for plugin " + pluginName)
				pluginPath := filepath.Join(vendorPluginPath, pluginName)
				pluginfilePath := filepath.Join(pluginPath, "Pluginfile")

				// Lookup Pluginfile
				if _, err := os.Stat(pluginfilePath); !os.IsNotExist(err) {
					// Pluginfile exists
					// Merge to config

					pluginConfig := viper.New()
					pluginConfig.SetConfigType("yaml")

					// read in environment variables that match
					pluginConfig.SetEnvPrefix(pluginName)
					pluginConfig.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
					pluginConfig.AutomaticEnv()

					log.Debug("Loading Pluginfile from " + pluginfilePath)
					pluginConfig.SetConfigFile(pluginfilePath)

					//err := cloudstackConfig.ReadInConfig()
					err = pluginConfig.MergeInConfig()
					CheckErr(err)

					// Update cloudstack config at plugins.PLUGIN_NAME with pluginConfig
					// We construct a copy of the Stackfile format to be able to properly MERGE in the plugin config
					// which we need to do to be able to override it again from the actual Stackfile
					// Rebuilding this:
					// plugins:
					//   PLUGIN_NAME:
					//     ...
					m := make(map[string]interface{})
					p := make(map[string]interface{})
					p[pluginName] = pluginConfig.AllSettings()
					m["plugins"] = p

					//cloudstackConfig.Set(strings.Join([]string{"plugins", pluginName}, "."), pluginConfig.AllSettings())
					cloudstackConfig.MergeConfigMap(m)
				}
			}
		}
	}
	// Load plugin configs
	var pluginDirContent []fs.FileInfo
	localPluginPath := filepath.Join(context, pluginRoot, plugin)
	if _, err := os.Stat(localPluginPath); !os.IsNotExist(err) {
		pluginDirContent, err = ioutil.ReadDir(localPluginPath)
		CheckErr(err)
		// Loop over plugins folder content
		for _, file := range pluginDirContent {
			if file.IsDir() {
				// This is a plugin!
				pluginName := file.Name()
				log.Debug("Loading config for plugin " + pluginName)
				pluginPath := filepath.Join(localPluginPath, pluginName)
				pluginfilePath := filepath.Join(pluginPath, "Pluginfile")

				// Lookup Pluginfile
				if _, err := os.Stat(pluginfilePath); !os.IsNotExist(err) {
					// Pluginfile exists
					// Merge to config

					pluginConfig := viper.New()
					pluginConfig.SetConfigType("yaml")

					// read in environment variables that match
					pluginConfig.SetEnvPrefix(pluginName)
					pluginConfig.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
					pluginConfig.AutomaticEnv()

					log.Debug("Loading Pluginfile from " + pluginfilePath)
					pluginConfig.SetConfigFile(pluginfilePath)

					//err := cloudstackConfig.ReadInConfig()
					err = pluginConfig.MergeInConfig()
					CheckErr(err)

					// Update cloudstack config at plugins.PLUGIN_NAME with pluginConfig
					// We construct a copy of the Stackfile format to be able to properly MERGE in the plugin config
					// which we need to do to be able to override it again from the actual Stackfile
					// Rebuilding this:
					// plugins:
					//   PLUGIN_NAME:
					//     ...
					m := make(map[string]interface{})
					p := make(map[string]interface{})
					p[pluginName] = pluginConfig.AllSettings()
					m["plugins"] = p

					//cloudstackConfig.Set(strings.Join([]string{"plugins", pluginName}, "."), pluginConfig.AllSettings())
					cloudstackConfig.MergeConfigMap(m)
				}
			}
		}
	}

	// read in environment variables that match
	cloudstackConfig.SetEnvPrefix("cloudstack")
	cloudstackConfig.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	cloudstackConfig.AutomaticEnv()

	log.Debug("Loading Stackfile from " + stackfile)
	cloudstackConfig.SetConfigFile(stackfile)

	//err := cloudstackConfig.ReadInConfig()
	err := cloudstackConfig.MergeInConfig()
	CheckErr(err)

	// https://github.com/spf13/viper/issues/188#issuecomment-399518663
	// for _, key := range viper.AllKeys() {
	// 	val := viper.Get(key)
	// 	viper.Set(key, val)
	// }

	// Verify config
	// Check if "plugins" key exists
	// This key is new in 2.0.0 (starting from 1.10.0)
	// All plugins are now sorted under this key
	// If it's missing, exit with notice to migrate config
	if !cloudstackConfig.IsSet("plugins") {
		log.Fatal("'plugins' key not found in configuration. Please migrate your configuration to the latest schema. For more information see https://docs.cloudstack.one/config-v2")
	}

	// Check if any of the legacy options is still configured
	legacyOptions := []string{
		"hcloud_csi",
		"hcloud_vms",
		"azure_aks",
		"k3s",
		"longhorn",
		"argocd",
		"letsencrypt",
		"cert_manager_crds",
		"eventrouter",
		"external_dns",
		"cilium_cni",
		"loki",
		"promtail",
		"nginx_ingress",
		"prometheus",
		"tempo",
		"portainer",
		"portainer_agent",
		"weave_scope",
		"keycloak",
		"kubeapps",
		"sonarqube",
		"metallb",
		"fission",
		"gitlab",
		"harbor",
		//
		"hcloud",
		"azure",
		"csi", // Dropped
		"ssh",
		"sshd",
		"route53",
		"cloudflare",
		"linkerd", // Dropped
		"slack",
		"alertmanager",
		"grafana",
		"paas", // Dropped
		"bastion",
		"stack.mail", // moved to plugins.letsencrypt.config.mail
	}

	var legacyOptionError bool = false
	for _, legacyOption := range legacyOptions {
		if cloudstackConfig.IsSet(legacyOption) {
			log.Error("Option '" + legacyOption + "' is depcrecated. Please migrate your configuration to the latest schema. For more information see https://docs.cloudstack.one/configuration/stackfile")
			legacyOptionError = true
		}
	}

	if legacyOptionError {
		log.Fatal("Legacy options found. Stopping.")
	}

	cloudstackConfig.BindPFlag("image.reference", rootCmd.Flags().Lookup("image-ref"))
	cloudstackConfig.BindPFlag("image.version", rootCmd.Flags().Lookup("image-version"))

	err = unmarshalStackfile()
	CheckErr(err)
}

func getTempFile(suffix string) (*os.File, error) {
	if suffix == "" {
		file, err := ioutil.TempFile("/tmp", "cloudstack"+cloudstack.Stack.Name+"-*."+suffix)
		return file, err
	} else {
		file, err := ioutil.TempFile("/tmp", "cloudstack"+cloudstack.Stack.Name+"-*.yml")
		return file, err
	}
}

func saveConfig() string {
	// Get temp path
	file, err := ioutil.TempFile("/tmp", "Stackfile."+cloudstackConfig.GetString("stack.name")+"-*.yml")
	CheckErr(err)
	// TODO: make this toggleable
	//defer os.Remove(file.Name())

	cloudstackConfig.WriteConfigAs(file.Name())
	return file.Name()
}

func saveRuntimeStackfile() {
	log.Debug("Setting runtime Stackfile: " + runtimeStackfile)

	// Get temp path
	file, err := ioutil.TempFile("/tmp", "Stackfile."+cloudstack.Stack.Name+"-*.yml")
	CheckErr(err)

	fileData, _ := yaml.Marshal(cloudstack)

	_ = ioutil.WriteFile(file.Name(), fileData, 0644)
	// TODO: make this toggleable
	//defer os.Remove(file.Name())

	//cloudstackConfig.WriteConfigAs(file.Name())
	runtimeStackfile = file.Name()
}

func configToEnv() []string {
	var env_vars []string
	for _, key := range cloudstackConfig.AllKeys() {
		var env_key_prefix string = "CLOUDSTACK"
		var env_key string = strings.ToUpper(strings.Join([]string{env_key_prefix, strings.ReplaceAll(key, ".", "_")}, "_"))
		var env_output string

		// Get value
		value := cloudstackConfig.Get(key)

		switch value := value.(type) {
		case []map[string]interface{}:
			log.Debug("Type: []map[string]interface{}")
			for index, listItem := range value {
				// Extend key
				_env_key := strings.Join([]string{env_key, fmt.Sprint(index)}, "_")

				for mapKey, mapValue := range listItem {
					switch mapValue := mapValue.(type) {
					case string:
						// Extend key
						_sub_key := strings.Join([]string{_env_key, fmt.Sprint(strings.ToUpper(mapKey))}, "_")
						env_output = strings.Join([]string{_sub_key, fmt.Sprint(mapValue)}, "=")
						env_vars = append(env_vars, []string{env_output}...)
					case map[string]string:
						_sub_key := strings.Join([]string{_env_key, fmt.Sprint(strings.ToUpper(mapKey))}, "_")
						for subMapKey, subMapValue := range mapValue {
							// Extend key
							_sub_sub_key := strings.Join([]string{_sub_key, fmt.Sprint(strings.ToUpper(subMapKey))}, "_")
							env_output = strings.Join([]string{_sub_sub_key, fmt.Sprint(subMapValue)}, "=")
							env_vars = append(env_vars, []string{env_output}...)
						}
					}
				}
			}
		case []map[string]string:
			log.Debug("Type: []map[string")
			for index, listItem := range value {
				// Extend key
				_env_key := strings.Join([]string{env_key, fmt.Sprint(index)}, "_")

				for mapKey, mapValue := range listItem {
					// Extend key
					_sub_key := strings.Join([]string{_env_key, fmt.Sprint(strings.ToUpper(mapKey))}, "_")
					env_output = strings.Join([]string{_sub_key, fmt.Sprint(mapValue)}, "=")
					env_vars = append(env_vars, []string{env_output}...)
				}
			}
		case []string:
			for index, listItem := range value {
				// Extend key
				_env_key := strings.Join([]string{env_key, fmt.Sprint(index)}, "_")
				env_output = strings.Join([]string{_env_key, fmt.Sprint(listItem)}, "=")
				env_vars = append(env_vars, []string{env_output}...)
			}
			log.Debug("Type: []string")
		default:
			log.Debug("Type: unknown")
			env_output = strings.Join([]string{env_key, fmt.Sprint(value)}, "=")
			env_vars = append(env_vars, []string{env_output}...)
		}

	}
	sort.Strings(env_vars)
	return env_vars
}

func unmarshalStackfile() error {
	validate = validator.New()
	err := cloudstackConfig.UnmarshalExact(&cloudstack)
	CheckErr(err)

	err = validate.Struct(cloudstack)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			log.Error(err)
			return nil
		}

		var errorMessage string

		for _, err := range err.(validator.ValidationErrors) {
			errorMessage = "Configuration option `" + strings.ToLower(err.Namespace()) + "` failed to validate: " + err.Tag()
		}

		// from here you can create your own error messages in whatever language you wish
		return goErrors.New(errorMessage)
	}
	return nil
}

func unmarshalDefaults() {
	log.Debug("Unmarshalling defaults")
	err := defaults.UnmarshalExact(&cloudstack)
	if err != nil {
		log.Fatal("unable to decode into struct, %v", err)
	}
}

func showConfig(format string) {
	if format == "json" {
		data, err := json.Marshal(cloudstack)
		CheckErr(err)
		fmt.Printf("%s\n", data)
	}
	if format == "yaml" {
		data, err := yaml.Marshal(cloudstack)
		CheckErr(err)
		fmt.Printf("%s\n", data)
	}
	if format == "env" {
		fmt.Println(cloudstackConfig.AllKeys())
	}

}

func unmarshalStackfile2(format string) string {
	var err error
	var bs []byte
	var output string

	c := cloudstackConfig.AllSettings()

	if format == "yaml" {
		bs, err = yaml.Marshal(c)
		if err != nil {
			log.Fatalf("unable to marshal config to "+format+": %v", err)
		}
		output = string(bs)
	} else if format == "env" {
		// TODO: implement env
		output = strings.Join(configToEnv(), "\n")
	} else {
		bs, err = yaml.Marshal(c)
		if err != nil {
			log.Fatalf("unable to marshal config to "+format+": %v", err)
		}
		output = string(bs)
	}
	return output
}

func checkKubernetesOnline(clientset *kubernetes.Clientset) bool {
	// Try to connect to the cluster
	_, err := clientset.ServerVersion()
	if err != nil {
		log.Warn("Could not connect to Kubernetes")
		k8sOnline = false
		return false
	} else {
		k8sOnline = true
		return true
	}
}

func getKubernetesClient() *kubernetes.Clientset {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil
	}

	if checkKubernetesOnline(clientset) {
		return clientset
	} else {
		return nil
	}

}

func discoverKubernetesDistro() {
	clientset := getKubernetesClient()

	if clientset != nil {
		log.Debug("Trying to get nodes from K8s")
		nodes, _ := clientset.CoreV1().Nodes().List(ctx.TODO(), metav1.ListOptions{})

		// if there's only 1 node, we can check for docker-desktop
		if len(nodes.Items) == 1 {
			// check if node's name is "docker-desktop"
			if nodes.Items[0].GetName() == "docker-desktop" {
				k8sDistro = "docker-desktop"
			}
		}
	}
}

func kubernetesDemo() {
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	// Cluster Info
	// cloudstack status
	// # IaaS - cloudstack iaas status
	// - Provider
	// # KaaS - cloudstack kaas status
	// - Provider
	// - CSI
	//   - Longhorn status
	// # PaaS - cloudstack paas status
	// - Nodes
	//   - Master
	//			- IP, Status, K8s-Version
	//   - Worker
	//			- IP, Status, K8s-Version
	// - Plugins (Table)
	//   - Name
	//   	 Version
	//   	 Status
	//   	 URL
	//   	 Admin-User
	//   	 Admin-Passwort

	for {
		pods, err := clientset.CoreV1().Pods("").List(ctx.TODO(), metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

		// Examples for error handling:
		// - Use helper functions like e.g. errors.IsNotFound()
		// - And/or cast to StatusError and use its properties like e.g. ErrStatus.Message
		namespace := "default"
		pod := "example-xxxxx"
		_, err = clientset.CoreV1().Pods(namespace).Get(ctx.TODO(), pod, metav1.GetOptions{})
		if errors.IsNotFound(err) {
			fmt.Printf("Pod %s in namespace %s not found\n", pod, namespace)
		} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
			fmt.Printf("Error getting pod %s in namespace %s: %v\n",
				pod, namespace, statusError.ErrStatus.Message)
		} else if err != nil {
			panic(err.Error())
		} else {
			fmt.Printf("Found pod %s in namespace %s\n", pod, namespace)
		}

		time.Sleep(10 * time.Second)
	}
}

func callPlugin(plugin string, pluginCommand string) error {
	log.Info("Calling plugin " + plugin + ", command " + pluginCommand)

	localPluginPath := filepath.Join(context, pluginRoot, plugin)
	containerPluginPath := filepath.Join("/context", pluginRoot, plugin)

	// Lookup plugin directory in context
	if _, err := os.Stat(localPluginPath); !os.IsNotExist(err) {
		// plugin path does exist
		log.Info("Found user plugin at " + localPluginPath)

		// // we handle this as a user plugin
		// // Loading plugin config
		// pluginConfig := viper.New()
		// pluginConfig.SetConfigFile(filepath.Join(localPluginPath, "Shipfile"))
		// pluginConfig.SetConfigType("yaml")

		// if err := pluginConfig.ReadInConfig(); err != nil {
		// 	if _, ok := err.(viper.ConfigFileNotFoundError); ok {
		// 		return goErrors.New("no Shipfile found in " + localPluginPath)
		// 	} else {
		// 		log.Error(err)
		// 	}
		// }

		// cloudstackConfig.Set("plugins."+plugin, pluginConfig)
		// unmarshalStackfile()

		// Set workdir
		workdir = filepath.Join(containerPluginPath)
		log.Debug("Changing workdir to " + workdir)
	}

	containerVendorPluginPath := filepath.Join("/plugins", plugin)

	// Lookup vendor plugin directory in container
	if _, err := os.Stat(containerVendorPluginPath); !os.IsNotExist(err) {
		// vendor plugin path does exist
		log.Info("Found vendor plugin at " + containerVendorPluginPath)

		// Set workdir
		workdir = filepath.Join(containerVendorPluginPath)
		log.Debug("Changing workdir to " + workdir)
	}

	// Load plugin config
	pluginConfig := cloudstack.Plugins[plugin]

	// Throw error if no commands are defined
	if pluginConfig.Commands == nil {
		return goErrors.New("No commands found for plugin " + plugin + ". Check if Stackfile.plugins." + plugin + " exists")
	}

	pluginCommands := cloudstack.Plugins[plugin].Commands

	// Throw error if no script or command exists
	if pluginCommands[pluginCommand].Script == nil {
		return goErrors.New("No configuration found for command " + pluginCommand + ". Check if Stackfile.plugins." + plugin + ".commands." + pluginCommand + " exists")
	}

	// Create tempfile for script
	scriptFile, err := ioutil.TempFile("/tmp", "cloudstack."+cloudstack.Stack.Name+"."+plugin+".script.*.sh")
	CheckErr(err)

	//defer os.Remove(scriptFile.Name())

	// Prepare script
	scriptSlice := []string{
		"#!/bin/bash",
		"set -euo pipefail",
	}

	script := append(scriptSlice, pluginCommands[pluginCommand].Script...) // viper.GetStringSlice("plugins."+plugin+".commands."+pluginCommand+".script"
	log.Debug(script)

	datawriter := bufio.NewWriter(scriptFile)

	for _, data := range script {
		_, _ = datawriter.WriteString(data + "\n")
	}

	datawriter.Flush()
	log.Debug("Saved script to " + scriptFile.Name())
	_ = os.Chmod(scriptFile.Name(), 0755)

	// Closing file descriptor
	// Getting fatal errors on windows WSL2 when accessing
	// the mounted script file from inside the container if
	// the file descriptor is still open
	// Works flawlessly with open file descriptor on M1 Mac though
	// It's probably safer to close the fd anyways
	scriptFile.Close()

	runCommand := []string{scriptFile.Name()}

	// Load default environment variables
	env := GetContainerEnv()
	env = append(env, strings.Join([]string{"CLOUDSTACK_SCRIPT_FILE", scriptFile.Name()}, "="))

	mounts := []string{
		strings.Join([]string{scriptFile.Name(), scriptFile.Name()}, ":"),
		strings.Join([]string{context, "/context"}, ":"),
		strings.Join([]string{kubeconfig, "/root/.kube/config"}, ":"),
		strings.Join([]string{runtimeStackfile, "/tmp/Stackfile"}, ":"),
	}

	// mount cloudstack development dir with --dev-dir
	if devDir != "" {
		mounts = append(mounts, strings.Join([]string{devDir, "/cloudstack"}, ":"))
	}

	// Check ports to open
	ports := []string{}

	if pluginCommand == "proxy" {
		proxyTargets := cloudstack.Plugins[plugin].Proxy

		for _, proxyTarget := range proxyTargets {
			ports = append(ports, strings.Join([]string{proxyTarget.Local_Port, proxyTarget.Local_Port}, ":"))
		}
	}

	if cloudstack.Plugins[plugin].Commands[pluginCommand].Interactive {
		interactive = true
	}

	exitCode, err := RunContainer(
		imageRef,
		imageVersion,
		runCommand,
		env,
		mounts,
		pull, // Pull
		workdir,
		ports,
	)

	if err != nil {

		log.Error("Plugin ", plugin, " failed with exit code ", exitCode, ": ", err.Error())
	} else {
		log.Info("Plugin ", plugin, " succeeded with exit code ", exitCode, ": OK")
	}

	return err
}

func yesNo() bool {
	prompt := promptui.Select{
		Label: "Select[Yes/No]",
		Items: []string{"Yes", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}

func promptGetInput(pc promptContent) string {
	validate := func(input string) error {
		if len(input) <= 0 {
			return goErrors.New(pc.errorMsg)
		}
		return nil
	}

	templates := &promptui.PromptTemplates{
		Prompt:  "{{ . }} ",
		Valid:   "{{ . | green }} ",
		Invalid: "{{ . | red }} ",
		Success: "{{ . | bold }} ",
	}

	prompt := promptui.Prompt{
		Label:     pc.label,
		Templates: templates,
		Validate:  validate,
	}

	result, err := prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		os.Exit(1)
	}

	return result
}

func promptGetSelect(pc promptContent) string {
	items := []string{"animal", "food", "person", "object"}
	index := -1
	var result string
	var err error

	for index < 0 {
		prompt := promptui.SelectWithAdd{
			Label:    pc.label,
			Items:    items,
			AddLabel: "Other",
		}

		index, result, err = prompt.Run()

		if index == -1 {
			items = append(items, result)
		}
	}

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		os.Exit(1)
	}

	return result
}

func promptYesNo(pc promptContent) string {
	items := []string{"Yes", "No"}
	index := -1
	var result string
	var err error

	for index < 0 {
		prompt := promptui.Select{
			Label: pc.label,
			Items: items,
		}

		index, result, err = prompt.Run()

		if index == -1 {
			items = append(items, result)
		}
	}

	if err != nil {
		log.Error("Prompt failed %v\n", err)
		os.Exit(1)
	}

	return result
}

func loadConfigFile(fp string, t string) (*viper.Viper, error) {
	config := viper.New()
	config.SetConfigFile(fp)
	config.SetConfigType(t)

	if err := config.ReadInConfig(); err != nil {
		return nil, err
	}
	return config, nil
}

func getCallUUID() string {
	id := uuid.New()
	return id.String()
}
